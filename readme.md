### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

This repository is a basic template for running a Laravel development with Docker

Reference: [Laravel with Docker Tutorial](https://kyleferg.com/laravel-development-with-docker/)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

Step 1: Install docker on your machine https://docs.docker.com/. After installing you should be able run commands with docker.

		`docker --version`
		
		`docker-compose --version`
		
		`docker ps` //shows runninng containers

Use `sudo` if you encounter issues


The docker configuration files are already setup on /deploy folder and docker-compose.yml. For more detailed information refer to the tutorial above.


Step 2: Run `docker-compose up -d` to start the containers. First time running it might take a few minutes to download the two base images.

Step 3: Make sure composer is installed then run `composer install` or `composer update`

Step 4: (optional) Run `docker-compose build` if prompted. Then check localhost:8080 to see check if everything's working.


Possible Issue

-Blank page on browser. This is caused by incorrect permission on /bootstrap and /storage directory.
To solve this run `chmod -R 777 directory-name`


Database with Docker

Step 6: Open your .env file and make sure your database credentials matches with your docker-compose.yml


To run commands inside your running container use `exec` (Docker 1.3)

Step 7: Run `docker ps` to get container ID of your app container, then `docker exec -it my-container-name /bin/bash` to enter container

Step 8: Run `php artisan migrate` to confirm database connection

Step 9: (optional) Tun `php artisan make:auth` to further test connection on browser

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact




## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb combination of simplicity, elegance, and innovation give you tools you need to build any application with which you are tasked.

## Learning Laravel

Laravel has the most extensive and thorough documentation and video tutorial library of any modern web application framework. The [Laravel documentation](https://laravel.com/docs) is thorough, complete, and makes it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 900 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
